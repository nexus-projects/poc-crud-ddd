package com.nexus.poc.api.app.pessoa.dto;

import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.util.message.MessageStack;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class PessoaConverterDTO {

    private final MessageStack messageStack;

    public PessoaDTO converter(final Pessoa pessoa) {
        log.debug("c=PessoaConverterDTO, m=converter, pessoa={}", pessoa);

        final var dto = new PessoaDTO();

        dto.setId(pessoa.getId());
        dto.setEmail(pessoa.getEmail());
        dto.setNome(pessoa.getNome());
        dto.setDataAtualizacao(pessoa.getDataAtualizacao());
        dto.setDataCriacao(pessoa.getDataCriacao());
        dto.setNascimento(pessoa.getNascimento());
        dto.setCidade(pessoa.getCidade());

        dto.setMensagem(messageStack.getMensagem());

        return dto;
    }
}
