package com.nexus.poc.domain.pessoa.infra;

import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.util.pagination.PaginationRequest;
import com.nexus.poc.util.pagination.ResponseList;

public interface IPessoaRepository {

    Pessoa deletarPorID(final Pessoa pessoa);

    Pessoa salvarPessoa(final Pessoa pessoa);

    Pessoa atualizarPessoa(final Pessoa pessoa);

    Pessoa alteraCidade(Pessoa pessoa);

    Pessoa buscarPorEmail(final String email);

    Pessoa buscarPorID(final Long id);

    ResponseList<Pessoa> buscarTodos(final PaginationRequest<Pessoa> pagination);

}
