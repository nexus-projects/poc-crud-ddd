package com.nexus.poc.util.pagination;

public enum SortDTO {

    ASC, DESC
}
