package com.nexus.poc.api.app.pessoa.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.util.message.Message;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PessoaDTO {

    private Long id;
    private String nome;
    private String email;
    private Message mensagem;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate nascimento;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dataCriacao;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dataAtualizacao;

    private Cidade cidade;

    public PessoaDTO(final Pessoa pessoa) {
        this.id = pessoa.getId();
        this.nome = pessoa.getNome();
        this.email = pessoa.getEmail();
        this.nascimento = pessoa.getNascimento();
        this.dataCriacao = pessoa.getDataCriacao();
        this.dataAtualizacao = pessoa.getDataAtualizacao();
        this.cidade = pessoa.getCidade();
    }
}
