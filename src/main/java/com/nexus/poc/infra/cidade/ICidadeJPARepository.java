package com.nexus.poc.infra.cidade;

import com.nexus.poc.domain.cidade.Cidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICidadeJPARepository extends JpaRepository<Cidade, Long> {

}
