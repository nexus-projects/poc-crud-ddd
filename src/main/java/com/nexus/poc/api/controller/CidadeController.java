package com.nexus.poc.api.controller;


import com.nexus.poc.api.app.cidade.CidadeApp;
import com.nexus.poc.api.app.cidade.dto.*;
import com.nexus.poc.api.app.pessoa.PessoaApp;
import com.nexus.poc.api.app.pessoa.dto.PessoaDTO;
import com.nexus.poc.api.app.pessoa.dto.PessoaFindDTO;
import com.nexus.poc.api.app.pessoa.dto.PessoaImputDTO;
import com.nexus.poc.api.app.pessoa.dto.PessoaUpdateDTO;
import com.nexus.poc.util.pagination.ResponseList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/cidade")
@Api(value = "[/cidade] - API Cidade", tags = {"Cidade"})
public class CidadeController {


    private final CidadeApp app;

    @PostMapping
    @ApiOperation(value = "Salvar Cidade", response = CidadeDTO.class, responseContainer = "CidadeDTO")
    public ResponseEntity<CidadeDTO> salvar(@RequestBody final CidadeImputDTO dto) {
        log.info("c=CidadeController, m=salvar, dto={}", dto);

        final var resultado = app.salvar(dto);

        return new ResponseEntity<>(resultado, CREATED);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "Atualizar Cidade", response = CidadeDTO.class, responseContainer = "CidadeDTO")
    public ResponseEntity<CidadeDTO> atualizar(@PathVariable Long id,  @RequestBody final CidadeUpdateDTO dto) {
        log.info("c=CidadeController, m=atualizar, id={}, dto={}", id, dto);

        final var resultado = app.atualizar(id, dto);

        return new ResponseEntity<>(resultado, OK);
    }

    @PutMapping(value = "/habitantes/{id}")
    @ApiOperation(value = "Alterar Qtd Habitantes", response = CidadeDTO.class, responseContainer = "CidadeDTO")
    public ResponseEntity<CidadeDTO> atualizarHabitantes(@PathVariable Long id, @RequestBody final CidadeUpdateQtdHabitantesDTO dto) {
        log.info("c=CidadeController, m=atualizarHabitantes, id={}, dto={}", id, dto);

        final var resultado = app.atualizarHabitantes(id, dto);

        return new ResponseEntity<>(resultado, OK);
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Buscar por ID", response = CidadeDTO.class, responseContainer = "CidadeDTO")
    public ResponseEntity<CidadeDTO> buscarPorID(@PathVariable Long id) {
        log.info("c=CidadeController, m=buscarPorID, id={}", id);

        final var resultado = app.buscarPorID(id);

        return new ResponseEntity<>(resultado, OK);
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "Deletar por ID", response = CidadeDTO.class, responseContainer = "CidadeDTO")
    public ResponseEntity<CidadeDTO> deletarPorID(@PathVariable Long id) {
        log.info("c=CidadeController, m=deletarPorID, id={}", id);

        final var resultado = app.deletarPorID(id);

        return new ResponseEntity<>(resultado, OK);
    }

    @GetMapping
    @ApiOperation(value = "Buscar Todos", response = ResponseList.class, responseContainer = "ResponseList")
    public ResponseEntity<ResponseList<CidadeDTO>> buscarTodos(CidadeFindDTO dto) {
        log.info("c=CidadeController, m=buscarTodos, dto={}", dto);

        final var resultado = app.buscarTodos(dto);

        return new ResponseEntity<>(resultado, OK);
    }
}
