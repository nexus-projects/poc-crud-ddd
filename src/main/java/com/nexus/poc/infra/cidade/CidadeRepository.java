package com.nexus.poc.infra.cidade;

import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.cidade.infra.ICidadeRepository;
import com.nexus.poc.util.pagination.PaginationRequest;
import com.nexus.poc.util.pagination.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import static java.util.Objects.nonNull;

@Repository
@Slf4j
@RequiredArgsConstructor
public class CidadeRepository implements ICidadeRepository {

    private final ICidadeJPARepository repository;

    @Override
    public Cidade deletarPorID(Cidade cidade) {
       log.debug("c=CidadeRepository, m=deletarPorID, cidade={}", cidade);

       repository.delete(cidade);

       return  cidade;
    }

    @Override
    public Cidade salvarCidade(Cidade cidade) {
        log.debug("c=CidadeRepository, m=salvarCidade, cidade={}", cidade);

        return repository.save(cidade);
    }

    @Override
    public Cidade alterarNumeroHabitantes(Cidade cidade) {
        log.debug("c=CidadeRepository, m=alteraNumeroHabitantes, cidade={}", cidade);
        return repository.save(cidade);
    }

    @Override
    public Cidade atualizarCidade(Cidade cidade) {
        log.debug("c=CidadeRepository, m=atualizarCidade, cidade={}", cidade);
        return repository.save(cidade);
    }

    @Override
    public Cidade buscarPorID(Long id) {
        log.debug("c=CidadeRepository, m=atualizarCidade, id={}", id);
        return repository.findById(id).orElse(null);
    }

    @Override
    public ResponseList<Cidade> buscarTodos(PaginationRequest<Cidade> handler) {
        log.debug("c=PessoaRepository, m=buscarTodos, where={}", handler);

        final Page<Cidade> page;

        if (nonNull(handler.getWhere())) {
            page = repository.findAll(handler.getWhere(), handler.getPage());
        } else {
            page = repository.findAll(handler.getPage());
        }

        return new ResponseList<>(page.getContent(), page.getNumber() + 1, page.getSize(), page.getTotalPages(), page.getTotalElements());
    }

}
