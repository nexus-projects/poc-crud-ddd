package com.nexus.poc.api.app.pessoa;

import com.nexus.poc.api.app.pessoa.dto.*;
import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.domain.pessoa.service.PessoaService;
import com.nexus.poc.util.pagination.PaginationRequest;
import com.nexus.poc.util.pagination.ResponseList;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static java.util.Objects.nonNull;

@Service
@Slf4j
@RequiredArgsConstructor
public class PessoaApp {

    private final PessoaConverterDTO conversor;
    private final PessoaService service;

    @Transactional
    public PessoaDTO salvarPessoa(final PessoaImputDTO dto) {
        log.debug("c=PessoaApp, m=salvarPessoa, dto={}", dto);

        final Pessoa resultado = service.salvarPessoa(dto.toModel());

        return conversor.converter(resultado);
    }

    @Transactional
    public PessoaDTO atualizarPessoa(final Long id, final PessoaUpdateDTO dto) {
        log.debug("c=PessoaApp, m=atualizarPessoa, id={}, dto={}", id, dto);

        final Pessoa resultado = service.atualizarPessoa(id, dto.toModel());

        return conversor.converter(resultado);
    }

    @Transactional
    public PessoaDTO atualizarCidade(final Long id, final PessoaUpdateCidadeDTO dto) {
        log.debug("c=PessoaApp, m=atualizarCidade, id={}, dto={}", id, dto);

        final Pessoa resultado = service.alteraCidade(id, dto.toModel());

        return conversor.converter(resultado);
    }

    @Transactional(readOnly = true)
    public PessoaDTO buscarPorID(final Long id) {
        log.debug("c=PessoaApp, m=buscarPorID, id={}", id);

        final Pessoa resultado = service.buscarPorID(id);

        return conversor.converter(resultado);
    }

    @Transactional(readOnly = true)
    public ResponseList<PessoaDTO> buscarTodos(final PessoaFindDTO dto) {
        log.debug("c=PessoaApp, m=buscarTodos, dto={}", dto);

        final var request = new PaginationRequest<>(dto.generateWhere(), dto.generatePagination());
        final var response = service.buscarTodos(request);
        final var data = converterDTO(response.getData(), dto);

        return new ResponseList<>(data, response);
    }

    @Transactional
    public PessoaDTO deletarPorID(final Long id) {
        log.debug("c=PessoaApp, m=deletarPorID, id={}", id);

        final Pessoa resultado = service.excluirPorID(id);

        return conversor.converter(resultado);
    }

    private List<PessoaDTO> converterDTO(final List<Pessoa> pessoas, final PessoaFindDTO dto) {
        final var fields = nonNull(dto.getFields()) ? dto.getFields() : new ArrayList<String>();

        final var dtos = pessoas.stream().map(p -> {
            final var builder = Pessoa.builder();

            builder.id(p.getId());

            if (fields.isEmpty() || fields.contains("nome")) {
                builder.nome(p.getNome());
            }

            if (fields.isEmpty() || fields.contains("email")) {
                builder.email(p.getEmail());
            }

            if (fields.isEmpty() || fields.contains("nascimento")) {
                builder.nascimento(p.getNascimento());
            }

            if (fields.isEmpty() || fields.contains("dataCriacao")) {
                builder.dataCriacao(p.getDataCriacao());
            }

            if (fields.isEmpty() || fields.contains("dataAtualizacao")) {
                builder.dataAtualizacao(p.getDataAtualizacao());
            }

            if (fields.isEmpty() || fields.contains("cidade")) {
                builder.cidade(p.getCidade());
            }

            return new PessoaDTO(builder.build());

        }).collect(Collectors.toList());

        return dtos;
    }

}
