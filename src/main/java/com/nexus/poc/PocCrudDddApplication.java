package com.nexus.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocCrudDddApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocCrudDddApplication.class, args);
	}

}
