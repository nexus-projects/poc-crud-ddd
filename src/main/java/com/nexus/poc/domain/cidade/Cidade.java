package com.nexus.poc.domain.cidade;

import lombok.*;

import javax.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;
import static javax.persistence.GenerationType.IDENTITY;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@Entity
@Table(name = "cidade")
public class Cidade {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "cidade_id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado")
    private EstadoEnum estado;

    @Column(name = "qtd_habitantes")
    private Integer qtdHabitantes;

    @Column(name = "distancia_capital")
    private BigDecimal distanciaCapital;


    @Column(name = "criado_em", updatable = false)
    private LocalDateTime dataCriacao;

    @Column(name = "alterado_em")
    private LocalDateTime dataAtualizacao;

    @PrePersist
    public void persist() {
        final var dataAtual = now();

        this.dataAtualizacao = dataAtual;
        this.dataCriacao = dataAtual;
    }

    @PreUpdate
    public void update() {
        this.dataAtualizacao = now();
    }

}
