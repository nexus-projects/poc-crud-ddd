package com.nexus.poc.domain.cidade.service;

import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.cidade.infra.ICidadeRepository;
import com.nexus.poc.util.error.ErrorMessage;
import com.nexus.poc.util.error.ErrorStack;
import com.nexus.poc.util.exception.NexusException;
import com.nexus.poc.util.exception.NexusNotFoundException;
import com.nexus.poc.util.message.MessageStack;
import com.nexus.poc.util.pagination.PaginationRequest;
import com.nexus.poc.util.pagination.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
@Slf4j
@RequiredArgsConstructor
public class CidadeService {

    private final ICidadeRepository repository;
    private final ErrorStack errorStack;
    private final MessageStack messageStack;

    public ResponseList<Cidade> buscarTodos(final PaginationRequest<Cidade> handler) {
        log.debug("c=CidadeService, m=buscarTodos, handler={}", handler);

        return repository.buscarTodos(handler);
    }

    public Cidade buscarPorID(final Long id) {
        log.debug("c=CidadeService, m=buscarPorID, id={}", id);

        final var pessoaCidade = repository.buscarPorID(id);

        if (isNull(pessoaCidade)) {
            NexusNotFoundException.of("Cidade não encontrada.");
        }

        return pessoaCidade;
    }

    public Cidade excluirPorID(final Long id) {
        log.debug("c=CidadeService, m=excluirPorID, id={}", id);

        final var resultado = repository.deletarPorID(this.buscarPorID(id));

        log.debug("Cidade {} excluída com sucesso", resultado);
        messageStack.addMensage("Cidade com id " + resultado.getId() + " excluída com sucesso.");

        return resultado;
    }

    public Cidade salvarCidade(final Cidade cidade) {
        log.debug("c=CidadeService, m=salvarCidade, cidade={}", cidade);

        if (!this.validar(cidade)) {
            log.error("Falha ao salvar cidade {}", cidade);
            NexusException.of("Falha ao salvar cidade", errorStack.getFalha());
        }

        final var cidadeSalva = repository.salvarCidade(cidade);
        log.debug("Cidade {} salva com sucesso", cidadeSalva);
        messageStack.addMensage("Cidade salva com sucesso.");

        return cidadeSalva;
    }

    public Cidade atualizarCidade(final Long id, final Cidade cidade) {
        log.debug("c=CidadeService, m=atualizarCidade, id={}, cidade={}", id, cidade);

        final var cidadeBusca = this.buscarPorID(id);

        final var cidadeAtualizar = cidadeBusca
                .toBuilder()
                .nome(cidade.getNome())
                .estado(cidade.getEstado())
                .qtdHabitantes(cidade.getQtdHabitantes())
                .distanciaCapital(cidade.getDistanciaCapital())
                .build();

        if (!this.validar(cidadeAtualizar)) {
            log.error("Falha ao salvar cidade {}", cidade);
            NexusException.of("Falha ao salvar cidade", errorStack.getFalha());
        }

        final var cidadeSalva = repository.atualizarCidade(cidadeAtualizar);
        log.debug("Cidade {} atualizada com sucesso", cidadeSalva);
        messageStack.addMensage("Cidade atualizada com sucesso.");

        return cidadeSalva;
    }

    public Cidade alterarNumeroHabitantes(final Long id, final Cidade cidade){
        log.debug("c=CidadeService, m=alterarNumeroHabitantes, id={}, cidade={}", id, cidade);

        final var buscarCidade = this.buscarPorID(id);

        final var cidadeAtualizar = buscarCidade
                .toBuilder()
                .qtdHabitantes(cidade.getQtdHabitantes())
                .build();

        if (!this.validar(cidadeAtualizar)) {
            log.error("Falha ao altrar qtdHabitantes da cidade {}", cidade);
            NexusException.of("Falha ao atualizar qtdHabitantes ", errorStack.getFalha());
        }

        final var cidadeSalva = repository.alterarNumeroHabitantes(cidadeAtualizar);
        log.debug("qtdHabitantes da Cidade {} atualizado com sucesso", cidadeSalva);
        messageStack.addMensage("qtdHabitantes atualizada com sucesso.");

        return cidadeSalva;
    }


    private Boolean validar(final Cidade cidade) {
        log.debug("c=CidadeService, m=validar, cidade={}", cidade);

        var validado = TRUE;

        if (isBlank(cidade.getNome())) {
            log.error("Nome é Obrigatório [{}]", cidade);
            errorStack.addError(new ErrorMessage("Nome é obrigatório"));

            validado = FALSE;
        }

        if (isNull(cidade.getEstado())) {
            log.error("Estado é Obrigatório [{}]", cidade);
            errorStack.addError(new ErrorMessage("Estado é obrigatório"));

            validado = FALSE;
        }

        if (isNull(cidade.getQtdHabitantes())) {
            log.error("Quantidade de Habitantes é Obrigatório [{}]", cidade);
            errorStack.addError(new ErrorMessage("Quantidade de Habitantes é Obrigatório"));

            validado = FALSE;
        }

        if (isNull(cidade.getDistanciaCapital())) {
            log.error("Distância da Capital é Obrigatório [{}]", cidade);
            errorStack.addError(new ErrorMessage("Distância da Capital é Obrigatório"));

            validado = FALSE;
        }

        return validado;
    }
}
