package com.nexus.poc.api.app.cidade.dto;

import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.cidade.EstadoEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CidadeImputDTO {

    private String nome;
    private EstadoEnum uf;
    private Integer qtdHab;
    private BigDecimal distancia;

    public Cidade toModel() {
        return Cidade
                .builder()
                .nome(this.nome)
                .estado(this.uf)
                .qtdHabitantes(this.qtdHab)
                .distanciaCapital(this.distancia)
                .build();
    }
}
