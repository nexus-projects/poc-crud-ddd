package com.nexus.poc.api.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import com.nexus.poc.api.app.pessoa.PessoaApp;
import com.nexus.poc.api.app.pessoa.dto.*;
import com.nexus.poc.util.pagination.ResponseList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/pessoa")
@Api(value = "[/pessoa] - API Pessoa", tags = {"Pessoa"})
public class PessoaController {

    private final PessoaApp app;

    @PostMapping
    @ApiOperation(value = "Salvar Pessoa", response = PessoaDTO.class, responseContainer = "PessoaDTO")
    public ResponseEntity<PessoaDTO> salvar(@RequestBody final PessoaImputDTO dto) {
        log.info("c=PessoaController, m=salvar, dto={}", dto);

        final var resultado = app.salvarPessoa(dto);

        return new ResponseEntity<>(resultado, CREATED);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "Atualizar Pessoa", response = PessoaDTO.class, responseContainer = "PessoaDTO")
    public ResponseEntity<PessoaDTO> atualizar(@PathVariable Long id,  @RequestBody final PessoaUpdateDTO dto) {
        log.info("c=PessoaController, m=atualizar, id={}, dto={}", id, dto);

        final var resultado = app.atualizarPessoa(id, dto);

        return new ResponseEntity<>(resultado, OK);
    }

    @PutMapping(value = "/cidade/{id}")
    @ApiOperation(value = "Atualizar Cidade Pessoa", response = PessoaDTO.class, responseContainer = "PessoaDTO")
    public ResponseEntity<PessoaDTO> atualizarCidade(@PathVariable Long id, @RequestBody final PessoaUpdateCidadeDTO dto) {
        log.info("c=PessoaController, m=atualizarCidade, id={}, dto={}", id, dto);

        final var resultado = app.atualizarCidade(id, dto);

        return new ResponseEntity<>(resultado, OK);
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Buscar por ID", response = PessoaDTO.class, responseContainer = "PessoaDTO")
    public ResponseEntity<PessoaDTO> buscarPorID(@PathVariable Long id) {
        log.info("c=PessoaController, m=buscarPorID, id={}", id);

        final var resultado = app.buscarPorID(id);

        return new ResponseEntity<>(resultado, OK);
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "Deletar por ID", response = PessoaDTO.class, responseContainer = "PessoaDTO")
    public ResponseEntity<PessoaDTO> deletarPorID(@PathVariable Long id) {
        log.info("c=PessoaController, m=deletarPorID, id={}", id);

        final var resultado = app.deletarPorID(id);

        return new ResponseEntity<>(resultado, OK);
    }

    @GetMapping
    @ApiOperation(value = "Buscar Todos", response = ResponseList.class, responseContainer = "ResponseList")
    public ResponseEntity<ResponseList<PessoaDTO>> buscarTodos(PessoaFindDTO dto) {
        log.info("c=PessoaController, m=buscarTodos, dto={}", dto);

        final var resultado = app.buscarTodos(dto);

        return new ResponseEntity<>(resultado, OK);
    }
}
