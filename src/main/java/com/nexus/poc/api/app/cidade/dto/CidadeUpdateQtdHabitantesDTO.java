package com.nexus.poc.api.app.cidade.dto;

import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.pessoa.Pessoa;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CidadeUpdateQtdHabitantesDTO {

    private Integer qtdHab;

    public Cidade toModel() {
        return Cidade
                .builder()
                .qtdHabitantes(this.qtdHab)
                .build();
    }
}
