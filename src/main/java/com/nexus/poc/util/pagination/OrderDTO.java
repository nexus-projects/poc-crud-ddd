package com.nexus.poc.util.pagination;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderDTO {

    private String field;
    private SortDTO sort;

}
