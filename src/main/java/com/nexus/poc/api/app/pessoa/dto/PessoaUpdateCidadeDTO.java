package com.nexus.poc.api.app.pessoa.dto;

import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.pessoa.Pessoa;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PessoaUpdateCidadeDTO {

    private Cidade cidade;


    public Pessoa toModel() {
        return Pessoa
                .builder()
                .cidade(this.cidade)
                .build();
    }
}
