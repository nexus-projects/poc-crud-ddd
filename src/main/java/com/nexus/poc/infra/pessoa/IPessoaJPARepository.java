package com.nexus.poc.infra.pessoa;

import com.nexus.poc.domain.pessoa.Pessoa;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPessoaJPARepository extends JpaRepository<Pessoa, Long> {

    Optional<Pessoa> findByEmail(final String email);

}
