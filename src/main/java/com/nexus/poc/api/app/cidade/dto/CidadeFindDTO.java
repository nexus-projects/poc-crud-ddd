package com.nexus.poc.api.app.cidade.dto;

import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.cidade.EstadoEnum;
import com.nexus.poc.util.pagination.FindAbstract;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CidadeFindDTO  extends FindAbstract<Cidade> {


    private String nome;
    private EstadoEnum uf;

    @Override
    protected Cidade getProbe() {
        return Cidade
                .builder()
                .nome(nome)
                .estado(uf)
                .build();
    }
}
