CREATE TABLE pessoa
(
    pessoa_id   serial PRIMARY KEY,
    nome        VARCHAR(50)        NOT NULL,
    email       VARCHAR(50) UNIQUE NOT NULL,
    nascimento  DATE               NOT NULL,
    created_on  TIMESTAMP          NOT NULL,
    last_update TIMESTAMP          NOT NULL
);