package com.nexus.poc.domain.cidade.infra;

import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.util.pagination.PaginationRequest;
import com.nexus.poc.util.pagination.ResponseList;

public interface ICidadeRepository {

    Cidade deletarPorID(final Cidade cidade);

    Cidade salvarCidade(final Cidade cidade);

    Cidade atualizarCidade(final Cidade cidade);

    Cidade alterarNumeroHabitantes(final Cidade cidade);

    Cidade buscarPorID(final Long id);

    ResponseList<Cidade> buscarTodos(final PaginationRequest<Cidade> pagination);
}
