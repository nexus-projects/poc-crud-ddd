package com.nexus.poc.api.app.pessoa.dto;

import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.util.pagination.FindAbstract;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PessoaFindDTO extends FindAbstract<Pessoa> {

    private String nome;
    private String email;

    @Override
    protected Pessoa getProbe() {
        return Pessoa
                .builder()
                .nome(nome)
                .email(email)
                .build();
    }
}
