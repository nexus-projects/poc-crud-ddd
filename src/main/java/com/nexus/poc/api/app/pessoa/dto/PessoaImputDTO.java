package com.nexus.poc.api.app.pessoa.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.pessoa.Pessoa;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PessoaImputDTO {

    private String nome;
    private String email;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate nascimento;

    private Cidade cidade;

    public Pessoa toModel() {
        return Pessoa
                .builder()
                .nome(this.nome)
                .email(this.email)
                .nascimento(this.nascimento)
                .cidade(this.cidade)
                .build();
    }
}
