CREATE TABLE IF NOT EXISTS cidade
(
    cidade_id  serial PRIMARY KEY,
    nome              VARCHAR(50) NOT NULL,
    estado            CHAR(2) NOT NULL,
    qtd_habitantes    INTEGER,
    distancia_capital NUMERIC,
    criado_em         TIMESTAMP NOT NULL,
    alterado_em       TIMESTAMP NOT NULL
);