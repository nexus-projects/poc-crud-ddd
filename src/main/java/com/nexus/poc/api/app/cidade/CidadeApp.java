package com.nexus.poc.api.app.cidade;

import com.nexus.poc.api.app.cidade.dto.*;
import com.nexus.poc.api.app.pessoa.dto.*;
import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.cidade.EstadoEnum;
import com.nexus.poc.domain.cidade.service.CidadeService;
import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.domain.pessoa.service.PessoaService;
import com.nexus.poc.util.pagination.PaginationRequest;
import com.nexus.poc.util.pagination.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
@Slf4j
@RequiredArgsConstructor
public class CidadeApp {

    private final CidadeConverterDTO conversor;
    private final CidadeService service;

    @Transactional
    public CidadeDTO salvar(final CidadeImputDTO dto) {
        log.debug("c=CidadeApp, m=salvar, dto={}", dto);

        final Cidade resultado = service.salvarCidade(dto.toModel());

        return conversor.converter(resultado);
    }

    @Transactional
    public CidadeDTO atualizar(final Long id, final CidadeUpdateDTO dto) {
        log.debug("c=CidadeApp, m=atualizar, id={}, dto={}", id, dto);

        final Cidade resultado = service.atualizarCidade(id, dto.toModel());

        return conversor.converter(resultado);
    }

    @Transactional
    public CidadeDTO atualizarHabitantes(final Long id, final CidadeUpdateQtdHabitantesDTO dto) {
        log.debug("c=CidadeApp, m=atualizarHabitantes, id={}, dto={}", id, dto);

        final Cidade resultado = service.alterarNumeroHabitantes(id, dto.toModel());

        return conversor.converter(resultado);
    }

    @Transactional(readOnly = true)
    public CidadeDTO buscarPorID(final Long id) {
        log.debug("c=CidadeApp, m=buscarPorID, id={}", id);

        final Cidade resultado = service.buscarPorID(id);

        return conversor.converter(resultado);
    }

    @Transactional(readOnly = true)
    public ResponseList<CidadeDTO> buscarTodos(final CidadeFindDTO dto) {
        log.debug("c=CidadeApp, m=buscarTodos, dto={}", dto);

        final var request = new PaginationRequest<>(dto.generateWhere(), dto.generatePagination());
        final var response = service.buscarTodos(request);
        final var data = converterDTO(response.getData(), dto);

        return new ResponseList<>(data, response);
    }

    @Transactional
    public CidadeDTO deletarPorID(final Long id) {
        log.debug("c=CidadeApp, m=deletarPorID, id={}", id);

        final Cidade resultado = service.excluirPorID(id);

        return conversor.converter(resultado);
    }

    private List<CidadeDTO> converterDTO(final List<Cidade> cidades, final CidadeFindDTO dto) {
        final var fields = nonNull(dto.getFields()) ? dto.getFields() : new ArrayList<String>();

        final var dtos = cidades.stream().map(p -> {
            final var builder = Cidade.builder();

            builder.id(p.getId());

            if (fields.isEmpty() || fields.contains("nome")) {
                builder.nome(p.getNome());
            }

            if (fields.isEmpty() || fields.contains("estado")) {
                builder.estado(p.getEstado());
            }

            if (fields.isEmpty() || fields.contains("qtdHabitantes")) {
                builder.qtdHabitantes(p.getQtdHabitantes());
            }
            if (fields.isEmpty() || fields.contains("distanciaCapital")) {
                builder.distanciaCapital(p.getDistanciaCapital());
            }

            if (fields.isEmpty() || fields.contains("dataCriacao")) {
                builder.dataCriacao(p.getDataCriacao());
            }

            if (fields.isEmpty() || fields.contains("dataAtualizacao")) {
                builder.dataAtualizacao(p.getDataAtualizacao());
            }

            return new CidadeDTO(builder.build());

        }).collect(Collectors.toList());

        return dtos;
    }
}
