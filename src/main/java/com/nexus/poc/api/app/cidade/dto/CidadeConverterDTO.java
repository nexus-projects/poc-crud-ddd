package com.nexus.poc.api.app.cidade.dto;

import com.nexus.poc.api.app.pessoa.dto.PessoaDTO;
import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.util.message.MessageStack;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class CidadeConverterDTO {

    private final MessageStack messageStack;

    public CidadeDTO converter(final Cidade cidade) {
        log.debug("c=CidadeConverterDTO, m=converter, cidade={}", cidade);

        final var dto = new CidadeDTO();

        dto.setCodigo(cidade.getId());
        dto.setNome(cidade.getNome());
        dto.setUf(cidade.getEstado());
        dto.setQtdHab(cidade.getQtdHabitantes());
        dto.setDistancia(cidade.getDistanciaCapital());
        dto.setMensagem(messageStack.getMensagem());

        return dto;
    }
}
