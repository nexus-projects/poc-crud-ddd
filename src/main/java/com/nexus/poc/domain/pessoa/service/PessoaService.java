package com.nexus.poc.domain.pessoa.service;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

import com.nexus.poc.domain.cidade.service.CidadeService;
import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.domain.pessoa.infra.IPessoaRepository;
import com.nexus.poc.util.error.ErrorMessage;
import com.nexus.poc.util.error.ErrorStack;
import com.nexus.poc.util.exception.NexusException;
import com.nexus.poc.util.exception.NexusNotFoundException;
import com.nexus.poc.util.message.MessageStack;
import com.nexus.poc.util.pagination.PaginationRequest;
import com.nexus.poc.util.pagination.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class PessoaService {

    private final IPessoaRepository repository;
    private final ErrorStack errorStack;
    private final MessageStack messageStack;
    private final CidadeService cidadeService;

    public ResponseList<Pessoa> buscarTodos(final PaginationRequest<Pessoa> handler) {
        log.debug("c=PessoaService, m=buscarTodos, handler={}", handler);

        return repository.buscarTodos(handler);
    }

    public Pessoa buscarPorID(final Long id) {
        log.debug("c=PessoaService, m=buscarPorID, id={}", id);

        final var pessoaBusca = repository.buscarPorID(id);

        if (isNull(pessoaBusca)) {
            NexusNotFoundException.of("Pessoa não encontrada.");
        }

        return pessoaBusca;
    }

    public Pessoa excluirPorID(final Long id) {
        log.debug("c=PessoaService, m=excluirPorID, id={}", id);

        final var resultado = repository.deletarPorID(this.buscarPorID(id));

        log.debug("Pessoa {} excluída com sucesso", resultado);
        messageStack.addMensage("Pessoa com id " + resultado.getId() + " excluída com sucesso.");

        return resultado;
    }

    public Pessoa salvarPessoa(final Pessoa pessoa) {
        log.debug("c=PessoaService, m=salvarPessoa, pessoa={}", pessoa);

        if (!this.validar(pessoa)) {
            log.error("Falha ao salvar pessoa {}", pessoa);
            NexusException.of("Falha ao salvar pessoa", errorStack.getFalha());
        }

        final var pessoaSalva = repository.salvarPessoa(pessoa);
        log.debug("Pessoa {} salva com sucesso", pessoaSalva);
        messageStack.addMensage("Pessoa salva com sucesso.");

        return pessoaSalva;
    }

    public Pessoa atualizarPessoa(final Long id, final Pessoa pessoa) {
        log.debug("c=PessoaService, m=atualizarPessoa, id={}, pessoa={}", id, pessoa);

        final var pessoaBusca = this.buscarPorID(id);

        final var pessoaAtualizar = pessoaBusca
                .toBuilder()
                .nome(pessoa.getNome())
                .email(pessoa.getEmail())
                .build();


        if (!this.validar(pessoaAtualizar)) {
            log.error("Falha ao salvar pessoa {}", pessoa);
            NexusException.of("Falha ao salvar pessoa", errorStack.getFalha());
        }

        final var pessoaSalva = repository.atualizarPessoa(pessoaAtualizar);
        log.debug("Pessoa {} atualizada com sucesso", pessoaSalva);
        messageStack.addMensage("Pessoa atualizada com sucesso.");

        return pessoaSalva;
    }

    public Pessoa alteraCidade(final Long id, final Pessoa pessoa) {

        final var buscaPessoa = this.buscarPorID(id);

        final var pessoaAtualizar = buscaPessoa
                .toBuilder()
                .cidade(pessoa.getCidade())
                .build();

        if (!this.validar(pessoaAtualizar)) {
            log.error("Falha ao Atualizar Pessoa {}", pessoaAtualizar);
            NexusException.of("Falha ao Atualizar Pessoa ", errorStack.getFalha());
        }

        final var pessoaSalva = repository.alteraCidade(pessoaAtualizar);
        log.debug("Pessoa atualizado com sucesso", pessoaSalva);
        messageStack.addMensage("Salva atualizado com sucesso.");
        return pessoaSalva;
    }

    private Boolean validar(final Pessoa pessoa) {
        log.debug("c=PessoaService, m=validar, pessoa={}", pessoa);

        var validado = TRUE;

        if (isBlank(pessoa.getNome())) {
            log.error("Nome Obrigatório [{}]", pessoa);
            errorStack.addError(new ErrorMessage("Nome obrigatório"));

            validado = FALSE;
        }

        if (isNull(pessoa.getCidade())) {
            log.error("Cidade é Obrigatória [{}]", pessoa);
            errorStack.addError(new ErrorMessage("Cidade obrigatório"));

            validado = FALSE;
        }

        if(nonNull(pessoa.getCidade())){
            final var cidadeExiste = cidadeService.buscarPorID(pessoa.getCidade().getId());
               if(isNull(cidadeExiste)){
                   validado = false;
               }
        }

        final var pessoaEmail = repository.buscarPorEmail(pessoa.getEmail());

        if (nonNull(pessoaEmail) && !pessoaEmail.getId().equals(pessoa.getId())) {
            log.error("E-mail {} já registrado para a pessoa {}", pessoa.getEmail(), pessoa);
            errorStack.addError(new ErrorMessage("E-mail já cadastrado."));

            validado = FALSE;
        }

        return validado;
    }
}
