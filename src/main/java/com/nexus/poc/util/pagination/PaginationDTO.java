package com.nexus.poc.util.pagination;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PaginationDTO {

    private Integer page;
    private Integer pageSize;
    private Integer pageTotal;
    private Long totalElements;

}
