package com.nexus.poc.api.app.pessoa.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nexus.poc.domain.pessoa.Pessoa;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PessoaUpdateDTO {

    private String nome;
    private String email;

    public Pessoa toModel() {
        return Pessoa
                .builder()
                .nome(this.nome)
                .email(this.email)
                .build();
    }
}
