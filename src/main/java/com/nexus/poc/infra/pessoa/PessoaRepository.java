package com.nexus.poc.infra.pessoa;

import static java.util.Objects.nonNull;
import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.domain.pessoa.infra.IPessoaRepository;
import com.nexus.poc.util.pagination.PaginationRequest;
import com.nexus.poc.util.pagination.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
@Slf4j
public class PessoaRepository implements IPessoaRepository {

    private final IPessoaJPARepository repository;

    @Override
    public Pessoa deletarPorID(final Pessoa pessoa) {
        log.debug("c=PessoaRepository, m=deletarPorID, pessoa={}", pessoa);

        repository.delete(pessoa);

        return pessoa;
    }

    @Override
    public Pessoa salvarPessoa(final Pessoa pessoa) {
        log.debug("c=PessoaRepository, m=salvarPessoa, pessoa={}", pessoa);

        return repository.save(pessoa);
    }

    @Override
    public Pessoa atualizarPessoa(final Pessoa pessoa) {
        log.debug("c=PessoaRepository, m=atualizarPessoa, pessoa={}", pessoa);

        return repository.save(pessoa);
    }

    @Override
    public Pessoa alteraCidade(Pessoa pessoa) {
        log.debug("c=PessoaRepository, m=alteraCidade, pessoa={}", pessoa);

        return repository.save(pessoa);
    }

    @Override
    public Pessoa buscarPorEmail(final String email) {
        log.debug("c=PessoaRepository, m=buscarPorEmail, email={}", email);

        return repository.findByEmail(email).orElse(null);
    }

    @Override
    public Pessoa buscarPorID(final Long id) {
        log.debug("c=PessoaRepository, m=buscarPorID, id={}", id);

        return repository.findById(id).orElse(null);
    }

    @Override
    public ResponseList<Pessoa> buscarTodos(final PaginationRequest<Pessoa> handler) {
        log.debug("c=PessoaRepository, m=buscarTodos, where={}", handler);

        final Page<Pessoa> page;

        if (nonNull(handler.getWhere())) {
            page = repository.findAll(handler.getWhere(), handler.getPage());
        } else {
            page = repository.findAll(handler.getPage());
        }

        return new ResponseList<>(page.getContent(), page.getNumber() + 1, page.getSize(), page.getTotalPages(), page.getTotalElements());
    }
}
