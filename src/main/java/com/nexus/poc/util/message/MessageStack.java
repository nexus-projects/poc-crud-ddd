package com.nexus.poc.util.message;

import lombok.Getter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
@Getter
public class MessageStack {

    private Message mensagem;

    public void addMensage(final String message) {
        this.mensagem = new Message(message);
    }
}
