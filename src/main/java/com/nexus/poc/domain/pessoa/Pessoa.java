package com.nexus.poc.domain.pessoa;

import static java.time.LocalDateTime.now;
import static javax.persistence.GenerationType.IDENTITY;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.*;

import com.nexus.poc.domain.cidade.Cidade;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@Entity
@Table(name = "pessoa")
public class Pessoa {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "pessoa_id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "email")
    private String email;

    @Column(name = "nascimento")
    private LocalDate nascimento;

    @Column(name = "created_on", updatable = false)
    private LocalDateTime dataCriacao;

    @Column(name = "last_update")
    private LocalDateTime dataAtualizacao;

    @ManyToOne
    @JoinColumn(name = "id_cidade")
    private Cidade cidade;

    @PrePersist
    public void persist() {
        final var dataAtual = now();

        this.dataAtualizacao = dataAtual;
        this.dataCriacao = dataAtual;
    }

    @PreUpdate
    public void update() {
        this.dataAtualizacao = now();
    }
}
