package com.nexus.poc.api.app.cidade.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.nexus.poc.domain.cidade.Cidade;
import com.nexus.poc.domain.cidade.EstadoEnum;
import com.nexus.poc.domain.pessoa.Pessoa;
import com.nexus.poc.util.message.Message;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CidadeDTO {

    private Long codigo;
    private String nome;
    private EstadoEnum uf;
    private Integer qtdHab;
    private BigDecimal distancia;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime criado;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime alterado;

    private Message mensagem;

    public CidadeDTO(final Cidade cidade) {
        this.codigo = cidade.getId();
        this.nome = cidade.getNome();
        this.uf = cidade.getEstado();
        this.qtdHab = cidade.getQtdHabitantes();
        this.distancia = cidade.getDistanciaCapital();
        this.criado = cidade.getDataCriacao();
        this.alterado = cidade.getDataAtualizacao();

    }
}
